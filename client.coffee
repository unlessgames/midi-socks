MENU_WIDTH = 60
COLORS = 
  darkest : "#222222"
  dark : "#555555"
  bright : "#888888"
  brightest : "#aaaaaa"
  clear : "#000000"
  fade : "#00000008"
DEBUG_LOGS = true

show_info = false
rack = []
current_module = 0
finger = null
fingers = []
font = null
is_mobile = /Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i.test(navigator.userAgent)
vid = document.getElementById("video")
dirty = true


socket = io(":8000")

socket.on "connect", () ->
  socket.on "cc", (m) -> feedback_param m.channel, m.controller, m.value

send = (type, message) ->
  # if DEBUG_LOGS
    # console.log type, message
  if type isnt "cc"
    message.note = message.controller
    message.velocity = 127
  socket.emit type, message
  dirty = true

center_rect = (x, y, w, h) ->
  rect(x - w * 0.5, y - h * 0.5, w, h)

get_width = () -> window.innerWidth - MENU_WIDTH

texter = (t, x, y, fs, c) ->
  fill c
  stroke c
  textSize fs
  text t, x, y

export_rack = () ->
  rack.map((m) ->m.params.map (p) -> p.message())
    # flatten compound message from xypad
    .map (m) ->
      m.reduce ((a,v,i) -> a.concat (if Array.isArray(v) then v else [v])), []
    .map (ps) -> ps.filter (p) -> p.value isnt 0

class v2
  constructor : (@x, @y) ->

class Box
  @empty : () -> new Box(0,0,1,1)
  set_center : () ->
    @center = new v2( @size.x * 0.5, @size.y * 0.5)
  constructor : (@x, @y, @w, @h) ->
    @pos = new v2(@x, @y)
    @size = new v2(@w, @h)
    @set_center()
  contains : (p) ->
    p.x > @pos.x && p.y > @pos.y && p.x < @pos.x + @size.x && p.y < @pos.y + @size.y

#_______________________________________________________________widgets

class Slider
  constructor : (@box, @id, @value = 0) ->
  message : () ->
    channel : @channel
    controller : @id
    value : @value
  set_value : (mouse) ->
    nt = constrain(floor(map(mouse.y, 0, height,127, 0)), 0, 127)
    if nt isnt @value
      @value = nt
      send "cc", @message()
  resize : (w, h) ->
    @box.set_size w / 8, h
  draw : (show_id) ->
    fill COLORS.darkest
    center_rect(@box.center.x, @box.center.y, @box.size.x - 10, @box.size.y - 10)
    fill(COLORS.dark)
    y = map(@value, 0, 127, height, 0)
    center_rect(@box.center.x, @box.center.y + y, @box.size.x - 10, @box.size.y)
    if show_id
      texter(@id, 
        @box.center.x, @box.center.y, 
        @box.size.x * 0.5, COLORS.brightest)

class XYPad
  constructor : (@box, @id, @grid, @x = 0, @y = 0, @pressed = false) ->
  single_message : (i, v) ->
    channel : @channel
    controller : i
    value : v
  message : () ->
    [
      @single_message @id, @x
      @single_message @id + 1, @y
      @single_message @id + 2, (if @pressed then 127 else 0)
    ]
  send_message : () ->
    ms = @message()
    for m in ms 
      send "cc", m
  set_value : (mouse) ->
    if not @pressed
      @pressed = true

    _x = constrain(floor(map(mouse.x, @box.x, @box.x + @box.w,0, 127)), 0, 127)
    if _x isnt @x
      @x = _x

    _y = constrain(floor(map(mouse.y,@box.y + @box.h, @box.y,0, 127)), 0, 127)
    if _y isnt @y
      @y = _y
    @send_message()
  resize : (w, h) ->
    @box = new Box(@box.x, @box.y, w, h)
  release : () ->
    if @pressed
      @pressed = false
      @send_message()
  draw : (show_id) ->
    fill(COLORS.dark)
    _x = map(@x, 127, 0, @box.size.x, 0)
    _y = map(@y, 0, 127, @box.size.y, 0)
    center_rect(_x, _y, 30.0, 30.0)
    if @grid?
      fill COLORS.dark
      if @grid.x != 0
        w = @box.w / @grid.x
        for i in [1..@grid.x - 1]
          rect(@box.x + i * w, @box.y, 2, @box.h)

      if @grid.y != 0
        h = @box.h / @grid.y
        for i in [1..@grid.y - 1]
          rect(@box.x, @box.y + h * i, @box.w, 2)



    if show_id
      stroke COLORS.brightest
      fill COLORS.brightest
      fs = 30
      textSize(fs)
      text("x : #{@id}", @box.center.x, @box.center.y - fs)
      text("y : #{@id + 1}", @box.center.x, @box.center.y)
      text("press : #{@id + 2}", @box.center.x, @box.center.y + fs)

class Pad
  send : (msg) ->
    send "cc", msg
  constructor : (@box, @id, @toggle = true, @up = false, @pressed = false) ->
  message : () ->
    channel : @channel
    controller : @id
    value : if @up then 127 else 0
  set_value : (mouse) ->
    if @box.contains mouse
      if not @pressed
        @pressed = true
        if @toggle
          @up = !@up
        else
          @up = true
        send "cc",@message()
    else
      @pressed = false
  release : () ->
    if not @toggle
      @up = false
      send "cc",@message()
    @pressed = false
  draw : (show_id) ->
    strokeWeight 2
    if @up
      fill COLORS.dark
      noStroke()
    else
      stroke COLORS.dark
      noFill()

    center_rect @box.center.x, @box.center.y, @box.size.x - 10, @box.size.y - 10
    if show_id
      fill(COLORS.brightest)
      textAlign(CENTER, CENTER)
      fs = @box.w * 0.2
      textSize(fs)
      text(@id, @box.center.x, @box.center.y)

# class PianoKey
#   constructor : (@box, @id, @up = false, @pressed = false) ->
#   message : () ->
#     channel : @channel
#     controller : @id
#     value : if @up then 127 else 0
#   set_value : (mouse) ->
#     if @box.contains mouse
#       if not @pressed
#         @pressed = true
#         @up = true
#         send "noteon", @message()
#     else
#       @pressed = false
#   release : () ->
#     if not @toggle
#       @up = false
#       send "noteoff", @message()
#     @pressed = false
#   draw : (show_id) ->
#     strokeWeight 2
#     if @up
#       fill COLORS.dark
#       noStroke()
#     else
#       stroke COLORS.dark
#       noFill()

#     center_rect @box.center.x, @box.center.y, @box.size.x - 10, @box.size.y - 10
#     if show_id
#       fill(COLORS.brightest)
#       textAlign(CENTER, CENTER)
#       fs = @box.w * 0.2
#       textSize(fs)
#       text(@id, @box.center.x, @box.center.y)

class Module
  constructor : (@params, @channel) ->
    for p in @params
      p.channel = @channel
  post_draw : () ->
  draw : () ->
    fill(if not @clear_color? then COLORS.clear else @clear_color)
    rect(0, 0, window.innerWidth - MENU_WIDTH, window.innerHeight)
    for p in @params
      push()
      translate(p.box.pos.x, p.box.pos.y)
      p.draw(show_info)
      pop()
    @post_draw()
  resize : (w, h) ->
    for p in @params
      p.resize w, h

class Finger
  constructor : (@selected = 0, @dragging = false) ->
  update : (params, mouse) ->
    if not @dragging
      for p, i in params
        if p.box.contains(mouse)
          @dragging = true
          @selected = i 
      false
    else
      params[@selected].set_value(mouse)
      true
  release : (params) ->
    if @dragging
      @dragging = false
      if params? and params[@selected].release?
        params[@selected].release()
      return true
    false

# Gyro = 
#   rotation : [0,0,0]
#   absolute : false
#   process : (d) ->
#     @rotation = [d.do.alpha, d.do.beta, d.do.gamma]
#     @absolute = d.do.absolute

# gn = new GyroNorm()

# gn.init().then(() ->
#   gn.start(Gyro.process.bind(Gyro))
# ).catch((e) -> console.log "no deviceorientation available")

# gyro_cc = (channel = 0, offset = 0) ->
#   refresh_rate = 10
#   names = ["alpha", "beta", "gamma"]
#   m = 
#     counter : 0
#     draw : () ->
#       push()
#       draw_axis = (n, v, i) ->
#         texter("#{n} : #{v}", 0, (i - 1) * 30, 30, COLORS.brightest)
#       counter = (counter + 1) % refresh_rate
#       w = get_width()
#       translate w * 0.5, window.innerHeight * 0.5
#       for i in [0..2]
#         draw_axis names[i], Gyro.rotation[i], i
#       pop()

#       # if counter is 0
#       #   for i in [0..2]
#           # socket.emit "cc", 
#           # draw_axis names[i], Gyro.rotation[i]
#     resize : () ->
#     params : []
#     channel : channel
# 06309350445
#   m




#_______________________________________________________________menu

Menu = 
  info_button : (h) ->
    if show_info
      fill COLORS.bright
    else
      noFill()
    stroke COLORS.bright
    y = h * rack.length + h * 0.5
    center_rect(@box.center.x, y, @box.size.x - 10, h - 10)
    textAlign(CENTER, CENTER)
    textSize(@box.size.x * 0.6)
    fill COLORS.brightest
    text("?", @box.center.x, y)
  box : new Box(window.innerWidth - MENU_WIDTH, 0, MENU_WIDTH, window.innerHeight)
  resize : () ->
    @box = new Box(window.innerWidth - MENU_WIDTH, 0, MENU_WIDTH, window.innerHeight)
  draw : () ->
    push()
    translate(@box.pos.x, @box.pos.y)
    fill COLORS.darkest
    noStroke()
    rect 0, 0, @box.w, @box.h
    h = @box.size.y / (rack.length + 1)
    for m, i in rack
      if i is current_module
        noStroke()
        fill COLORS.bright
      else 
        noFill()
        stroke COLORS.bright

      center_rect(@box.center.x, h * i + h * 0.5, @box.size.x - 10, h - 10)
      b = new Box(8, h * i + 8, @box.w - 16, h - 16)
      if m.icon?
        m.icon b
      if show_info
        textSize b.w * 0.8
        fill COLORS.brightest
        stroke COLORS.brightest
        text(m.channel + 1, b.x + b.center.x, b.y + b.center.y)

    Menu.info_button(h)
    pop()

#_______________________________________________________________main p5

preload = () ->
  font = loadFont("Terminus.ttf")


setup = () ->
  finger = new Finger()
  fingers = [0..9].map((i) -> new Finger())
  textFont(font)
  textAlign CENTER, CENTER  
  createCanvas(window.innerWidth, window.innerHeight)

dirty = true

draw = () ->
  update()

  if dirty
    dirty = false
    noStroke()
    rack[current_module].draw()
    Menu.draw()
    if show_info
      fs = 20
      texter "midi-socks by unless games", fs * 7, height - fs, fs, COLORS.brightest 


update = () ->
  params = rack[current_module].params
  if touches.length > 0
    for t in touches
      m = createVector(t.x, t.y)
      fingers[t.id].update params, m
  else
    mouse = createVector(mouseX, mouseY)
    if mouseIsPressed
      finger.update params, mouse
    else
      finger.release()

    for f in fingers
      f.release()

switch_to = (i) ->
  clear(COLORS.clear)
  for p in rack[current_module].params
    if p.release? then p.release
  current_module = i

mouseClicked = () ->
  mouse = createVector(mouseX, mouseY)
  if Menu.box.contains mouse
    nt = floor(map(mouseY, 0, height, 0, rack.length + 1))
    if nt is rack.length
      show_info = !show_info
      if is_mobile and not fullscreen()
        fullscreen(true)
        document.getElementById("video").play()
    else
      switch_to nt

  dirty = true
      
windowResized = () ->
  # window.location.reload()
  resizeCanvas(window.innerWidth, window.innerHeight)
  for m in rack
    m.resize(get_width(), window.innerHeight)
  Menu.resize()
  dirty = true

mouseReleased = () ->
  params = rack[current_module].params
  finger.release params
  for f in fingers
    f.release params
  dirty = true

#_______________________________________________________________modules

sliders = (channel = 0, offset = 0, count = 8) ->
  m = new Module([0..count - 1].map(
    (i) -> 
      new Slider(
        Box.empty()
        i + offset
      )
  ), channel)
  m.resize = (w, h) ->
    w = get_width() / count
    h = window.innerHeight
    # console.log w
    for p, i in @params
      p.box = new Box(w * i, 0, w, h)
  m.resize()
  m.post_draw = () ->
    fill COLORS.dark
    noStroke()
    center_rect get_width() / 2, window.innerHeight / 2, 2, window.innerHeight
  m.icon = (b) ->
    w = b.w / count
    for i in [0..count - 1]
      fill COLORS.dark
      noStroke()
      center_rect b.x + w * i + w * 0.5, b.y + b.h * 0.5, w - 3, b.h
  m

xypad = ( channel = 0, offset = 0) ->
  m = new Module([
    new XYPad( new Box(0, 0, get_width(), window.innerHeight), offset)
  ], channel)
  m.clear_color = COLORS.fade
  m.icon = (b) ->
    fill COLORS.dark
    stroke COLORS.dark
    textSize b.w * 0.5
    text "xy", b.x + b.center.x, b.y + b.center.y
  m


dualxypad = ( channel = 0, offset = 0) ->
  m = new Module([
    new XYPad( Box.empty(), offset)
    new XYPad( Box.empty(), offset + 3)
  ], channel)
  m.clear_color = COLORS.fade
  m.resize = () ->
    mw = get_width()
    @params[0].box = new Box(0,0,mw/2,window.innerHeight)
    @params[1].box = new Box(mw/2,0,mw/2,window.innerHeight)
  m.resize()
  m.post_draw = () ->
    fill COLORS.dark
    noStroke()
    center_rect get_width() / 2, window.innerHeight / 2, 2, window.innerHeight
  m.icon = (b) ->
    fill COLORS.dark
    stroke COLORS.dark
    fs = b.w * 0.2
    textSize fs * 2
    text "2", b.x + b.center.x, b.y + b.center.y - fs
    text "xy", b.x + b.center.x, b.y + b.center.y + fs
  m

keys = (channel = 0, offset = 0, count = 24) ->
  black_keys = [false, true, false, true, false, false, true, false, true, false, true, false]
  indices = [0, 0, 1, 1, 2, 3, 3, 4, 4, 5, 5, 6]

  key_box = (i, o = 0) ->
    is_black = black_keys[i % 12]
    h = window.innerHeight / 2
    w = get_width() / 14
    oct = if i >= 12 then get_width() / 2 else 0
    x = oct + (indices[i % 12]) * w + (if is_black then w / 2 else 0)
    y = if is_black then 0 else h
    new Box(x, y, w, h)

  m = new Module(
    [0..count - 1].map((i) -> new PianoKey(Box.empty(), i)),
    channel
  )
  m.resize = (w, h) ->
    for i in [0..count-1]
      @params[i].box = key_box(i)
  m.resize()
  m.icon = (b) ->
    fill COLORS.dark
    stroke COLORS.dark
    textSize b.w * 0.5
    text "pk", b.x + b.center.x, b.y + b.center.y
  m

pads4x4 = ( channel = 0, offset = 0) ->
  pad_box = (i) ->
    h = window.innerHeight
    mw = window.innerWidth - MENU_WIDTH
    w = Math.min(h, mw) / 4
    _x = mw * 0.5 - 2 * w
    x = i % 4
    y = Math.floor(i / 4)
    _y = if h > mw then h * 0.5 - 2 * w else 0
    new Box(_x + x * w, _y + y * w, w, w)
  ps = [0..15].map((i)->new Pad(pad_box(i), offset + i))
  m = new Module(ps, channel)
  m.resize = () ->
    for p, i in @params
      @params[i].box = pad_box i
  m.icon = (b) ->
    w = b.w / 4
    h = b.h / 4
    noStroke()
    fill(COLORS.dark)
    for x in [0..3]
      for y in [0..3]
        center_rect(b.x + x * w + w / 2, b.y + y * h + h / 2, w - 2, h - 2)
  m

pads4x4momentary = ( channel = 0, offset = 0) ->
  m = pads4x4 channel, offset
  for p in m.params
    p.toggle = false
  icon = m.icon
  m.icon = (b) ->
    icon(b)
    fill COLORS.darkest
    textSize(b.w * 0.9)
    text("m", b.x + b.center.x, b.y + b.center.y - 5)
  m

#_______________________________________________________________your rack

rack = [
  pads4x4(0)
  pads4x4momentary(1)
  sliders(2)
  sliders(2, 8)
  xypad(3)
  dualxypad(3, 3)
]


feedback_param = (channel, index, value) ->
  for m,mi in rack
    if m.channel is channel
      for p, pi in m.params
        if p.id is index
          rack[mi].params[pi].value = value
          dirty = true
  # m = rack.filter((m) -> m.channel is channel and m.params.find((p) -> p.id is index)?)

### todo
  # gyro_cc(0)
  fix pad reentry
  add gyro mode
  haptic feedback
  optimize drawing
###
