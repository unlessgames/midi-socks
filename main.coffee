fs = require 'fs'
ip = require 'ip'
express = require 'express'
app = express()
url = require 'url'
http = require('http').Server(app)
io = require('socket.io')(http)
port = process.env.PORT || 8000
app.use(express.static(__dirname + "/"))
http.listen(port, () ->)

qrcode = require('qrcode-terminal');

midi = require('easymidi')
output = new midi.Output("midi-socks", true);
input = new midi.Input("midi-socks", true);

socket = null

input.on "cc", (m) -> 
  if socket? then socket.emit("cc", m)
# input.on "noteon", (m) -> 
#   console.log m
# input.on "noteoff", (m) -> 
#   console.log m
# input.on("noteon", (m) ->  if socket? then socket.emit("noteon", m))
# input.on("noteoff", (m) ->  if socket? then socket.emit("noteoff", m))


io.on("connection", (s) ->
  console.log "connected to #{s.conn.remoteAddress}"
  socket = s

  s.on("cc", (m) -> output.send("cc", m))
  s.on("noteon", (m) -> output.send("noteon", m))
  s.on("noteoff", (m) -> output.send("noteoff", m))

  s.on("disconnect", () -> 
    console.log "disconnected from #{s.conn.remoteAddress}"
    s = null
  )
)

console.log "midi-socks started.\n"


console.log "-> go to http://#{ip.address()}:#{port} on your phone or browser\n"
qrcode.generate("http://#{ip.address()}:#{port}");
